package com.example.integration.demo.mvc;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller implements InitializingBean {

    @Autowired
    private MeterRegistry meterRegistry;

    private Counter hitCount;

    @GetMapping("/test")
    public String getResponse() {
        hitCount.increment();
        return "Response returned from new version!";
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        hitCount = this.meterRegistry.
                counter("http.hit-count", "test",
                        "counter"); // 1 - create a counter
    }
}
